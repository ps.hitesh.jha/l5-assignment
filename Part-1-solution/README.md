# Part 1



### linux:


Create a full solution that achieves the below requirements knowing that it contains three parts:

#### Part1:

1. Create three virtual machines using Vmware or Oracle Virtual box.

2. Change the hostname for both of three VMs to be server1, server2, and server3.

3. Enable ssh on all the VMs.

4. Add a Password for the GRUB and set it as "P@ssw0rd".

5. Create a script that reads the disk usage and save the result into a file, name this script as “disk-stat.sh”.

6. Make this script on your PC to connect to 3 server and execute same script every 5 mins and writes the output on you PC.

hit : the output must be like below :

```

  output of server1 :

  Filesystem      Size  Used Avail Use% Mounted on
  tmpfs           2.4G  3.2M  2.4G   1% /run
  efivarfs        246K   71K  171K  30% /sys/firmware/efi/efivars
  /dev/nvme0n1p2  445G  408G   15G  97% /

  output of server2 :

  Filesystem      Size  Used Avail Use% Mounted on
  /dev/nvme0n1p2  700G  408G   15G  88% /

  output of server3 :

  Filesystem      Size  Used Avail Use% Mounted on
  /dev/nvme0n1p2  1000G  428G   20G  9% /  

```

7. Send the statistics (Disk usage) from the three virtual machines to your laptop and by email.


#### Hint for question7: you can use fake SMTP. 

# **Solution:**

Mostly the provisioning of the VM is handled from **dis-stat.sh**. It i using a vagrant script to setup 3 VMs to be deployed over OracleVM. Execute below to provisition the VMs

```
$ cd ./l5-assignment/Part-1-solution

$ vagrant up
```
Once the VMs are provisioned you will see it running on OracleVM as below:

![Screenshot 1](images/image1.png)

Next we need to execute a script that will fetch the disk usage from all three servers and save it to a file named **disk_usage.txt** 

After that we have SMTP service from google which will send email to specified users. 
Run below script:

```
./disk-stat.sh
```
You will notice something below once script is executed:

![Screenshot 2](images/image2.png)

Below shows the emails sent after every 5 mins to specified email accounts along with the attachment for disk usage:

![Screenshot 3](images/image3.png)

![Screenshot 4](images/image4.png)



