#!/bin/bash

execute_remote_script() {
    server=$1
    vagrant ssh $server -c 'touch /vagrant/disk_usage.txt && \
                           echo "Output of '${server}' : " >> /vagrant/disk_usage.txt && \
                           echo >> /vagrant/disk_usage.txt && \
                           { df -hH | grep Filesystem; df -hH | grep -v Filesystem | sort -rh -k4 | head -n 5; } >> /vagrant/disk_usage.txt && \
                           echo >> /vagrant/disk_usage.txt'
}

# Array of Vagrant server IPs
servers=("server1" "server2" "server3")

# Loop indefinitely, execute script every 5 minutes
while true; do
    # Execute script on each server
    for server in "${servers[@]}"; do
        execute_remote_script $server


    done
    bash smtp/smtp.sh disk_usage.txt
    echo "Wait for 5 minutes"
    sleep 300
done


