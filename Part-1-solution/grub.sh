#!/bin/bash
# Install required package for generating GRUB password hash
sudo apt-get update
sudo apt-get install -y grub2-common

sudo sed -i 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=5/' /etc/default/grub
sudo update-grub

# Generate password hash for "P@ssw0rd"
HASH=$(echo -e "P@ssw0rd\nP@ssw0rd" | sudo grub-mkpasswd-pbkdf2 | grep "grub.pbkdf2.sha512.10000." | cut -d " " -f 7)

# Set GRUB password
echo 'set superusers="root"' | sudo tee -a /etc/grub.d/40_custom
echo "password_pbkdf2 root $HASH" | sudo tee -a /etc/grub.d/40_custom

# Update GRUB
sudo update-grub
