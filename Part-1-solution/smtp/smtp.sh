#!/bin/bash
# User input
# app password: smtp---lobp evsi cbkf wtdk
# https://www.geeksforgeeks.org/send-mails-using-a-bash-script/
#read -p "Enter your email : " sender
#read -p "Enter recipient email : " receiver
#read -p "Enter your Google App password : " gapp
#
#read -p "Enter the subject of mail : " sub
#
#echo "Enter the body of mail : "
#cat > tempfile.txt                # using cat command for multiline input
body=$(cat smtp/email-body.txt)          # storing the content of file in a variable
#body = "This is an automated email sent from Hitesh Jha as a art of L5 assignment."

#rm tempfile.txt

sender="hitesh.jha415@gmail.com"
receiver="hitesh.jha@clusus.com"

gapp=`(echo -n bG9icGV2c2ljYmtmd3Rkaw== | base64 -d)` #"lobpevsicbkfwtdk"
sub="L5 assignment from u899/Please do not reply to this email"
#body="This is test email from Hitesh Jha's L5-assignement to test the SMPT server from google"


# check for provided attachment file as a positional parameter
# -z indicating an empty positional parameter
# attachment doesn't exist condition

if [ -z "$1" ]; then


# curl command for accessing the smtp server

    curl -s --url 'smtps://smtp.gmail.com:465' --ssl-reqd \
    --mail-from $sender \
    --mail-rcpt $receiver\
    --user $sender:$gapp \
     -T <(echo -e "From: ${sender}
To: ${receiver}
Subject:${sub}

 ${body}")


# attachment exists condition
else

    file="$1"           # set file as the 1st positional parameter

    # MIME type for multiple type of input file extensions

    MIMEType=`file --mime-type "$file" | sed 's/.*: //'`
    curl -s --url 'smtps://smtp.gmail.com:465' --ssl-reqd \
    --mail-from $sender \
    --mail-rcpt $receiver\
    --user $sender:$gapp \
     -H "Subject: $sub" -H "From: $sender" -H "To: $receiver" -F \
    '=(;type=multipart/mixed' -F "=$body;type=text/plain" -F \
      "file=@$file;type=$MIMEType;encoder=base64" -F '=)'

fi

echo email sent