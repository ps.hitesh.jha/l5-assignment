
# **Installing Samba**

**To install Samba, we run:**

```
$ sudo apt update
$ sudo apt install samba
```

# **Setting up Samba**
Now that Samba is installed, we need to create a directory for it to share:
```
mkdir /home/vagrant/sambashare/
```

The command above creates a new folder sambashare in our home directory which we will share later.

The configuration file for Samba is located at /etc/samba/smb.conf. 

To add the new directory as a share, we edit the file by running:

```
sudo nano /etc/samba/smb.conf
```

At the bottom of the file, add the following lines:

```
[sambashare]
comment = Samba on Ubuntu
path = /home/username/sambashare
read only = no
browsable = yes
```

path: The directory of our share.

read only: Permission to modify the contents of the share folder is only granted when the value of this directive is no.

browsable: When set to yes, file managers such as Ubuntu’s default file manager will list this share under “Network” (it could also appear as browseable).

Now that we have our new share configured, save it and restart Samba for it to take effect:

```
sudo service smbd restart
```

Update the firewall rules to allow Samba traffic:

```
sudo ufw allow samba
```

# **Setting up User Accounts and Connecting to Share**
   
Since Samba doesn’t use the system account password, we need to set up a Samba password for our user account:

```
sudo smbpasswd -a vagrant
```

Connecting to Share

On Ubuntu: Open up the default file manager and click Connect to Server then enter

![Screenshot 5](images/image5.png)

![Screenshot 6](images/image6.png)


