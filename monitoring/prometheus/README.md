## Deploy prometheus using docker 

Create a persistent volume. Containers are ephemeral, including the data they store on their local file system. This means that if you run Prometheus in a container, all of your monitoring data vanishes when that container is shut down.

```
docker volume create prometheus-volume
```

#### Run Prometheus

```
docker run --rm --detach \
    --name my-prometheus \
    --publish 9094:9090 \
    --volume prometheus-volume:/prometheus \
    --volume "$(pwd)"/prometheus.yml:/etc/prometheus/prometheus.yml \
     prom/prometheus
```

The prometheus web-ui should be Up and running as below:

![Screenshot 13](images/image13.png)

#### Tomcat integration with prometheus

Downloaded the java exporter from github repo (https://github.com/prometheus/jmx_exporter/releases).

We simply need to add the following arguments to the JVM startup command:

```	
-javaagent:jmx_prometheus_javaagent-0.20.0.jar=9115:tomcat-config.yaml
```

If we split the argument by the semi column, we have these:

First argument tells JVM to consider this the instrumentation setting. Second is the path to the binary which will listen on port 9115. Third, and last, is the path to the Java exporter configuration file.

Next, we simply add a new job in scrape_config section of Prometheus configuration file:

```
  - job_name: tomcat
    static_configs:
      - targets: ["{HOSTNAME or IP}:9115"]
```
 Once done restart the promethuescontainer for the configuration reload. We can see the that tomcat metrics are getting collected.

![Screenshot 15](images/image15.png)

![Screenshot 16](images/image16.png)



#### USEFUL LINKS

https://squaredup.com/blog/three-ways-to-run-prometheus/

https://www.dbi-services.com/blog/apache-tomcat-monitoring-in-prometheus-without-jmx/
