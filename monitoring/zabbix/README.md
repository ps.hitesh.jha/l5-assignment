![Screenshot 9](images/image9.png)

Official documentation: https://www.zabbix.com/documentation/current/en/manual/installation/containers

Zabbix is an enterprise-class open source distributed monitoring solution.

Zabbix is software that monitors numerous parameters of a network and the health and integrity of servers. Zabbix uses a flexible notification mechanism that allows users to configure e-mail based alerts for virtually any event. This allows a fast reaction to server problems. Zabbix offers excellent reporting and data visualisation features based on the stored data. This makes Zabbix ideal for capacity planning.

### Installation from containers

abbix provides Docker images for each Zabbix component as portable and self-sufficient containers to speed up deployment and update procedure.



#### Run Zabbix server with MySQL database support, Zabbix web interface based on the Nginx web server and Zabbix Java gateway.

1. Create network dedicated for Zabbix component containers:
```
docker network create --subnet 172.20.0.0/16 --ip-range 172.20.240.0/20 zabbix-net
```

2. Start empty MySQL server instance
```
sudo docker run --name mysql-server -t \
-e MYSQL_DATABASE="zabbix" \
-e MYSQL_USER="zabbix" \
-e MYSQL_PASSWORD="zabbix_pwd" \
-e MYSQL_ROOT_PASSWORD="root_pwd" \
--network=zabbix-net \
--restart unless-stopped \
-d mysql:8.0-oracle \
--character-set-server=utf8 --collation-server=utf8_bin \
--default-authentication-plugin=mysql_native_password
```

3. Start Zabbix Java gateway instance

```
sudo docker run --name zabbix-java-gateway -t \
      --network=zabbix-net \
      --restart unless-stopped \
      -d zabbix/zabbix-java-gateway:alpine-6.4-latest
```

4. Start Zabbix server instance and link the instance with created MySQL server instance

```
sudo docker run --name zabbix-server-mysql -t \
      -e DB_SERVER_HOST="mysql-server" \
      -e MYSQL_DATABASE="zabbix" \
      -e MYSQL_USER="zabbix" \
      -e MYSQL_PASSWORD="zabbix_pwd" \
      -e MYSQL_ROOT_PASSWORD="root_pwd" \
      -e ZBX_JAVAGATEWAY_ENABLE="true" \
      -e ZBX_JAVAGATEWAY="zabbix-java-gateway" \
      -e ZBX_JAVAGATEWAYPORT=10052 \
      -e ZBX_STARTJAVAPOLLERS=5 \
      --network=zabbix-net \
      -p 10051:10051 \
      --restart unless-stopped \
      -d zabbix/zabbix-server-mysql:alpine-6.4-latest```
```

5. Start Zabbix web interface and link the instance with created MySQL server and Zabbix server instances

```
sudo docker run --name zabbix-web-nginx-mysql -t \
      -e ZBX_SERVER_HOST="zabbix-server-mysql" \
      -e DB_SERVER_HOST="mysql-server" \
      -e MYSQL_DATABASE="zabbix" \
      -e MYSQL_USER="zabbix" \
      -e MYSQL_PASSWORD="zabbix_pwd" \
      -e MYSQL_ROOT_PASSWORD="root_pwd" \
      --network=zabbix-net \
      -p 80:8080 \
      --restart unless-stopped \
      -d zabbix/zabbix-web-nginx-mysql:alpine-6.4-latest

```

6. Start Zabbix agent container in zabbix

```
sudo docker run --name zabbix-agent \
    -e ZBX_SERVER_HOST="zabbix-server-mysql" \
    --restart=always \
    --network=zabbix-net \
    -d zabbix/zabbix-agent:latest

```

7. Start Zabbix agent 2 container for monitoring the database

```
sudo docker run --name zabbix-agen2 \
    -e ZBX_SERVER_HOST="1f9361ff2d2e" \
    --restart=always \
    --network=zabbix-net \
    -d zabbix/zabbix-agent2:latest```
```

Once the services are up and running, we can view the dashboard directly on port(:80)

Note: Default username/password is Admin/zabbix

![Screenshot 10](images/image10.png)

Once we enter the login credentials, we can view the zabbix dashboard

### Let us monitor one of PS product(ECC) which is deployed over tomcat. We need to integrate it with zabbix

Add following variables in the tomcat/bin/catalina.sh where ECC is deployed and start the ECC tomcat

```
CATALINA_OPTS="$CATALINA_OPTS -Dcom.sun.management.jmxremote.port=10052"
CATALINA_OPTS="$CATALINA_OPTS -Dcom.sun.management.jmxremote.ssl=false"
CATALINA_OPTS="$CATALINA_OPTS -Dcom.sun.management.jmxremote.authenticate=false"

```

### Next we need to add the host in zabbix in order to view the real time data from tomcat:

1. On zabbix dashboard, navigate to page Monitoring/Hosts
2. Add the tomcat host(eg: tomcat)
3. Select one the groups, these are just for zabbix for logical grouping of the hosts, select Linux servers for now.
4. Add in the interface, enter the server IP and port and ensure that it matches to the one configured on tomcat(-Dcom.sun.management.jmxremote.port) as shown below.

![Screenshot 12](images/image12.png)

5. Once the host is added, we can now view the associated tomcat metrics and logs as below:

![Screenshot 7](images/image7.png)

6. I have not gone through all the features, but from above we can monitor general threads, memory, GC and other metrics which can be used for capacity planning. 

Feel free to explore more services by zabbix

